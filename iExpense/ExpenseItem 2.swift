//
//  expenseitem.swift
//  iExpense
//
//  Created by Marcel Mravec on 01.02.2022.
//

import Foundation

struct ExpenseItem {
    let name: String
    let type: StringLiteralType
    let amount: Double
}
