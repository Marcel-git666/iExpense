//
//  expenseitem.swift
//  iExpense
//
//  Created by Marcel Mravec on 01.02.2022.
//

import Foundation

struct ExpenseItem: Identifiable, Codable {
    var id = UUID()
    let name: String
    let type: StringLiteralType
    let amount: Double
}
