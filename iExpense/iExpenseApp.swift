//
//  iExpenseApp.swift
//  iExpense
//
//  Created by Marcel Mravec on 30.01.2022.
//

import SwiftUI

@main
struct iExpenseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
